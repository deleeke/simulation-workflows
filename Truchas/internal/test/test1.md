Steps for (re)constructing test case #1
Model: disk_out_ref.smtk

Enclosure
* Disable!

Functions
* Al viscosity
* (1)  662, 1379
* (2)  669, 1364
* (3)  769, 1339
* (4)  685, 1324
* (5)  689, 1317
* (6)  700, 1268
* (7)  718, 1250
* (8)  768, 1175
* (9)  806, 1102
* (10)  833, 1028

* Source Function
* 100, 100.001
* 200, 200.002
* 300, 300.003

Material
* Name "Aluminum 6061-T6"
* Two Phase
* Density 2700
* Conductivity 167
* Viscosity function "Al viscosity"
* Thermal specificaton "Specific Heat" 
* Specific heat 896
* Latent heat 321
* Solidus Temperature 582
* Liquidus Temperature 652
* Associated to block id 1

Material
* Name "Void Material"
* Void
* Temperature 20

Background material
* Set to "Void material"


Surfaces: Thermal Surface Conditions
* Name "Thermal BC"
* Type DS Boundary Condition
* Condition "HTC and/or External Radiation"
* HTC: Heat Transfer Coefficient (h) 0.543
* HTC: Reference Temperature (T0) 212.5
* External: Emissitivity (epsilon) 0.888
* External: Ambient Temp (T infinity) 32.2
* Associated to set id 1

Surface: Thermal Surface Conditions
* Name "Thermal Interface"
* Type DS Interface Condition
* Condition "Internal HTC"
* Heat Transfer coefficient 0.123
* Associated to set id 4

Surface: Fluid Boundary Conditions
* Name "Pressure BC"
* Variable "Pressure"
* Boundary condition type "Dirichlet"
* Value 73.73
* Inflow checked
* Inflow material "Aluminum 6061-T6"
* Inflow temperature 707.7
* Associated to set id 2

Surface: Fluid Boundary Conditions
* Name "Velocity BC"
* Variable Velocity
* Boundary Condition Type Dirichlet
* Velocity Subgroup 0 1.1 2.2 3.3
* Associated to set id 3

Solver: Analysis Type
* Analysis Type "Fluid Plus Thermal"
* Thermal plus fluid: Thermal solver: Vebose Stepping ON
* Fluid Only: Flow numerics: Viscous Number 5
* Fluid Only: Flow Numerics: Mass limiter ON; cutoff 2.5e-5 (advanced)
* Viscous Flow Model enable

Solver: Simulation Control
* Enable
* Phase Start Times Add 2 values 0, 20
* Phase Init Dt Factor 0.01

Body
* Name "body-0" (default)
* Temperature 98.6
* Associated to set id 1

Sources
* Name "Heat source"
* Temperature function "Source function"
* Associated to set 1

Other
# Exodus Block Modulus enabled (advanced)

* Start Time 0
* End Time 999.9
* Output Delta-Time Multiplier 0.02

* In advanced - create 2 output sub groups:
* 1, 0.1
* 5, 0.2

* Fluid Body Force: 0, 0, -32.2

* Probes
* Name "Probe 1"
* Description: Descriptive phrase goes here...
* Coordinates 3.14159, -0.111, 2.54
