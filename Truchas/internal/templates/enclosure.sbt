<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Heat Transfer</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="enclosure-radiation" Label="Enclosure Radiation" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="enable" Label="Enable" Version="0" Optional="true" IsEnabledByDefault="true">
          <ItemDefinitions>
            <Group Name="chaparral" Label="Chaparral" Version="0">
              <ItemDefinitions>
                <Void Name="blocking-enclosure" Label="Blocking Enclosure" Version="0" Optional="true" IsEnabledByDefault="true">
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Void>
                <Double Name="partial-enclosure-area" Label="Partial Enclosure Area" Version="0" Optional="true" IsEnabledByDefault="false">
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Int Name="bsp-max-tree-depth" Label="BSP Maximum Tree Depth" Version="0" AdvanceLevel="1">
                  <DefaultValue>10</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Int Name="bsp-min-leaf-length" Label="BSP Min Leaf Length" Version="0" AdvanceLevel="1">
                  <DefaultValue>25</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Double Name="spatial-tolerance" Label="Spatial Tolerance" Version="0">
                  <DefaultValue>1.0e-8</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Int Name="hemicube-resolution" Label="Hemicube Resolution" Version="0" AdvanceLevel="1">
                  <DefaultValue>60</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Double Name="min-separation" Label="Min Separation" Version="0" AdvanceLevel="1">
                  <DefaultValue>15.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                  </RangeInfo>
                </Double>
                <Int Name="max-subdivisions" Label="Max Subdivisions" Version="0" AdvanceLevel="1">
                  <DefaultValue>60</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Double Name="smoothing-tolerance" Label="Smoothing Tolerance" Version="0" AdvanceLevel="1">
                  <DefaultValue>1.0e-8</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Int Name="smoothing-max-iter" Label="Smoothing Max Iterations" Version="0" AdvanceLevel="1">
                  <DefaultValue>100</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Int Name="smoothing-weight" Label="Smoothing Weight" Version="0" AdvanceLevel="1">
                  <DefaultValue>2.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Int Name="verbosity-level" Label="Verbosity Level" Version="0">
                  <DiscreteInfo DefaultIndex="0">
                    <Value Enum="No Output">0</Value>
                    <Value Enum="Basic">1</Value>
                    <Value Enum="Frequent Output">2</Value>
                  </DiscreteInfo>
                </Int>
              </ItemDefinitions>
            </Group>
            <Group Name="enclosure" Label="Enclosure" Version="0">
              <ItemDefinitions>
                <String Name="enclosure-name" Label="Enclosure Name" Version="0">
                  <DefaultValue>furnace</DefaultValue>
                </String>
                <Group Name="symmetries" Label="Symmetries" Version="0" Extensible="true" NumberOfRequiredGroups="0">
                  <ItemDefinitions>
                    <String Name="symmetry" Label="Symmetry/Periodicity" Version="0">
                      <ChildrenDefinitions>
                        <Int Name="folds" Label="Number of Folds" Version="0"></Int>
                      </ChildrenDefinitions>
                      <DiscreteInfo DefaultIndex="0">
                        <Value Enum="Mirror X">MirrorX</Value>
                        <Value Enum="Mirror Y">MirrorY</Value>
                        <Value Enum="Mirror Z">MirrorZ</Value>
                        <Structure>
                          <Value Enum="Periodic Rotate X">RotX</Value>
                          <Items>
                            <Item>folds</Item>
                          </Items>
                        </Structure>
                        <Structure>
                          <Value Enum="Periodic Rotate Y">RotY</Value>
                          <Items>
                            <Item>folds</Item>
                          </Items>
                        </Structure>
                        <Structure>
                          <Value Enum="Periodic Rotate Z">RotZ</Value>
                          <Items>
                            <Item>folds</Item>
                          </Items>
                        </Structure>
                      </DiscreteInfo>
                    </String>
                  </ItemDefinitions>
                </Group>
                <ModelEntity Name="ignore-block-ids" Label="Ignore Blocks" Version="0" Extensible="true" NumberOfRequiredValues="0">
                  <MembershipMask>volume</MembershipMask>
                </ModelEntity>
                <Int Name="moving-radiation" Label="Moving Radiation?" Version="0">
                  <ChildrenDefinitions>
                    <!-- Children for moving radiation-->
                    <Group Name="displacement-sequence" Label="Displacement Sequence" Version="0" Extensible="true" NumberOfRequiredValues="0">
                      <ItemDefinitions>
                        <Double Name="displacement" Label="Displacement Vector" Version="0" NumberOfRequiredValues="4">
                          <ComponentLabels>
                            <Label>time:</Label>
                            <Label>x:</Label>
                            <Label>y:</Label>
                            <Label>z:</Label>
                          </ComponentLabels>
                        </Double>
                      </ItemDefinitions>
                    </Group>
                    <String Name="enclosure-file-prefix" Label="Enclosure File Prefix" Version="0"></String>
                    <Void Name="linear-interpolation" Label="Use Linear Interpolation" Version="0" Optional="true" IsEnabledByDefault="true"></Void>
                    <!-- Children for stationary radiation-->
                    <Void Name="skip-geometry-check" Label="Skip Geometry Check" Version="0" Optional="true" IsEnabledByDefault="false"></Void>
                    <File Name="enclosure-file" Label="Enclosure File" Version="0" NumberOfRequiredValues="1"></File>
                  </ChildrenDefinitions>
                  <DiscreteInfo DefaultIndex="1">
                    <Structure>
                      <Value Enum="No (Stationary)">0</Value>
                      <Items>
                        <Item>skip-geometry-check</Item>
                        <Item>enclosure-file</Item>
                      </Items>
                    </Structure>
                    <Structure>
                      <Value Enum="Yes (Moving)">1</Value>
                      <Items>
                        <Item>displace-side-sets</Item>
                        <Item>displacement-sequence</Item>
                        <Item>enclosure-file-directory</Item>
                        <Item>enclosure-file-prefix</Item>
                        <Item>linear-interpolation</Item>
                      </Items>
                    </Structure>
                  </DiscreteInfo>
                </Int>
                <!-- Children common to moving & stationary radiation-->
                <Double Name="ambient-temperature" Label="Ambient Temperature" Version="0">
                  <DefaultValue>0.0</DefaultValue>
                  <ExpressionType>tabular-function</ExpressionType>
                </Double>
                <Double Name="error-tolerance" Label="Radiosity Solver Tolerance" Version="0" AdvanceLevel="1">
                  <DefaultValue>1.0e-4</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
