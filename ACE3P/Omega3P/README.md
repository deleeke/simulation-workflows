Omega3P is one solver in the ACE3P simulation suite.
Omega3P is a frequency domain solver for computing
resonant modes (with damping).

More information on Omega3P is at
https://confluence.slac.stanford.edu/display/AdvComp/Omega3P
